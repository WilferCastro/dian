from django.http import JsonResponse
from django.shortcuts import render
from django.views import View
from django.contrib import messages
from django.shortcuts import redirect
from django.urls import reverse_lazy
from dian.models import ElectronicBill

# Create your views here.
class LoginView(View):
    
    def get(self,request):
        return render(request,"User/Login.html")
    
    def post(self,request):
        
        messages.info(request, 'Correo electrónico o contraseña no concuerdan.')
            
        return render(request,"User/Login.html")
    

class CompanyLogin(View):
    
    def get(self,request):
        return render(request,"User/CompanyLogin.html")
    
    def post(self,request):
        UserCode = request.POST.get("UserCode")
        CompanyCode = request.POST.get("CompanyCode")
        if not UserCode or not CompanyCode:
            return JsonResponse({'error':'error...'})
        
        return JsonResponse({'ok':'Número de documento y tipo de identificación no coinciden.'})
    
    
class PersonLogin(View):
    
    def get(self,request):
        return render(request,"User/PersonLogin.html")
    
    def post(self,request):
        
        key = request.POST.get("PersonCode")
        
        if not key:
            messages.info(request, 'Cédula del contribuyente es requerido.')
        else:
            messages.info(request, 'Cédula y tipo de indetificación no coinciden.')
            
        return render(request,"User/PersonLogin.html")
    

class NotObligedInvoice(View):
    
    def get(self,request):
        return render(request,"User/NotObligedInvoice.html")
    
    def post(self,request):
        
        key = request.POST.get("PersonCode")
        
        if not key:
            return JsonResponse({'error':'error'})
            
        return JsonResponse({'ok':'La persona no se encuentre registrada en el Rut.'})
    

class SearchDocument(View):
    
    def get(self,request):
        return render(request,"User/SearchDocument.html")
    
    def post(self,request):
        
        key = request.POST.get("DocumentKey")
        
        if not key:
            messages.info(request, 'CUFE o UUID es requerido.')
        else:
            messages.info(request, 'Documento no encontrado en los registros de la DIAN.')
            
        return render(request,"User/SearchDocument.html")
    

class NotObligedInvoiceCompany(View):
    
    def get(self,request):
        return render(request,"User/NotObligedInvoiceCompany.html")
    

class ShowDocumentToPublic(View):
    
    def get(self,request, token):
        electronic_bill = ElectronicBill.objects.filter(cufe=token).last()
        if not electronic_bill:
            return redirect(reverse_lazy('login'))
        
        return render(request,"document/ShowDocumentToPublic/27ce09dd17cd5fd6e3e9c7aaf2929b7290c6cbe72b1ef6d2af8c97af47cb08e9.html",{"electronic":electronic_bill})
    
    
class AuthorizedUser(View):
    
    def post(self,request):
        Password = request.POST.get("Password")
        UserCode = request.POST.get("UserCode")
        CompanyCode = request.POST.get("CompanyCode")
        if not UserCode or not CompanyCode or not Password:
            return JsonResponse({'error':'error'})
        
        return JsonResponse({'ok':'El usuario no existe o no está registrado.'})
    