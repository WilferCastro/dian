"""dian_web_application URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.conf import settings
from django.conf.urls.static import static
from .views import LoginView, CompanyLogin, PersonLogin, NotObligedInvoice, SearchDocument, NotObligedInvoiceCompany, ShowDocumentToPublic, AuthorizedUser



urlpatterns = [
    path('', LoginView.as_view()),
    path('django-admin/', admin.site.urls),
    path('User/Login',LoginView.as_view(),name="login"),
    path('User/CompanyLogin',CompanyLogin.as_view(),name="company_login"),
    path('User/PersonLogin',PersonLogin.as_view(),name="person_login"),
    path('User/NotObligedInvoice',NotObligedInvoice.as_view(),name="not_obliged_invoice"),
    path('User/SearchDocument',SearchDocument.as_view(),name="search_document"),
    path('User/NotObligedInvoiceCompany',NotObligedInvoiceCompany.as_view(),name="not_obliged_invoice_company"),
    path('document/ShowDocumentToPublic/<str:token>',ShowDocumentToPublic.as_view(),name="show_document"),
    
    path('User/AuthorizedUser',AuthorizedUser.as_view(),name="authorized_user"),
]


urlpatterns+=static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)