from django.contrib import admin
from .models import ElectronicBill


@admin.register(ElectronicBill)
class ElectronicBillAdmin(admin.ModelAdmin):
    list_display = ('cufe','fecha_emision',)

