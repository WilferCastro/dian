from django.db import models


class ElectronicBill(models.Model):
    cufe = models.CharField(max_length=100,verbose_name="TOKEN")
    enlace = models.CharField(null=True,max_length=200,default="http://127.0.0.1:8000/document/ShowDocumentToPublic/",verbose_name="ENLACE WEB")
    serie = models.CharField(max_length=30,default="EF",verbose_name="SERIE")
    folio = models.CharField(max_length=30,default="77",verbose_name="FOLIO")
    fecha_emision = models.DateField(verbose_name="FECHA DE EMISION")
    nit_emisor = models.CharField(max_length=45,default="901447155",verbose_name="NIT DEL EMISOR")
    nombre_emisor = models.CharField(max_length=45,default="MARTINEZ RINCON FABIO",verbose_name="NOMBRE DEL EMISOR")
    nit_receptor = models.CharField(max_length=45,default="8301015281")
    nombre_receptor = models.CharField(max_length=45,default="GOLD AND GREEN STONES SAS",verbose_name="NOMBRE DEL RECEPTOR")
    iva = models.CharField(max_length=30,default="0")
    total = models.CharField(max_length=45,verbose_name="TOTAL")
    legitimo_tenedor = models.CharField(max_length=45,default="MARTINEZ RINCON FABIO",verbose_name="NOMBRE DEL LEGITIMO TENEDOR")
    pdf = models.FileField(null=True,blank=True,verbose_name="PDF")
    
    def __str__(self):
        return self.cufe
    
    class Meta:
        verbose_name = 'Factura Electrónica'
        verbose_name_plural = 'Facturas Electrónicas'
